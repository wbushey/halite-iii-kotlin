#!/bin/sh

set -e

./gradlew shadowJar
./halite --replay-directory replays/ -vvv --width 32 --height 32 "java -jar build/libs/MyBot.jar" "java -jar build/libs/MyBot.jar"
