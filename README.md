# Halite III Bot

## Run in local game

`./run_game.sh`

## CLI

The Halite executable comes with a command line interface (CLI). Run `$ ./halite --help` to see a full listing of available flags.

## Submitting the bot

* `./gradlew zip` to create a zip with a jar that can be executed by the Halite servers
* Submit zipped file in `build/distributions` to: https://halite.io/play-programming-challenge

## Custom compilation on our game servers
* Your bot has `10 minutes` to install dependencies and compile on the game server.
* You can run custom commands to set up your bot by including an `install.sh` file alongside `MyBot.{ext}`. This file will be executed and should be a Bash shell script. You must include the shebang line at the top: `#!/bin/bash`.
  * For Python, you may install packages using pip, but you may not install to the global package directory. Instead, install packages as follows: `python3.6 -m pip install --system --target . numpy`
* Some languages don't use the `MyBot.{ext}` convention. Exceptions include:
  * Rust: a Cargo.toml in the root will be detected as Rust. Your bot will compile with `cargo rustc`.
  * Swift: a Package.swift in the root will be detected as Swift. Your bot will compile with `swift build`.
  * Haskell: You may upload a MyBot.hs, or you may upload a `stack.yaml`, in which case your bot will compile with `stack build`.
  * Elixir: Upload a mix.exs. Your bot will compile with `mix deps.get` followed by `mix escript.build`.
  * Clojure: Upload a project.clj. Your bot will compile with `lein uberjar`.
  * .NET: Upload a MyBot.csproj or MyBot.fsproj. Your bot will compile with `dotnet restore` followed with `dotnet build`.

## Acknowledgements

This Kotlin Halite starter repo builds upon the [Kotlin starter kit provided by Two Sigma](https://halite.io/learn-programming-challenge/downloads).
